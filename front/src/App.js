import React, {Component, Fragment} from 'react';
// import {Route, Switch} from "react-router-dom";
import {Card, CardBody} from "reactstrap";
import {connect} from "react-redux";
import {createMessage, fetchMessages} from "./store/actions/messages";
import MessageThumbnail from "./components/MessageThumbnail/MessageThumbnail";
import {Link} from "react-router-dom";
import MessageForm from "./components/MessageForm/MessageForm";

class App extends Component {
    componentDidMount() {
        this.props.onFetchMessages();
    }

    render() {
        // console.log(this.props.messages);
        return (
            <Fragment>
                <h2>
                    Messages
                </h2>

                <MessageForm onSubmit={this.props.postMessage}/>
                {this.props.messages.map((message,key) => (
                    <Card key={key}>

                        <CardBody>
                            {!message.image ? null :<MessageThumbnail image={message.image}/>}
                            <Link to={"/messages/" + message.id}>
                                {console.log(message)}
                                <h3>author:{message.author ? message.author:<span>anon</span> }</h3>
                            </Link>
                            <Link to={"/messages/" + message.id}>
                                <h1>messaga: {message.message}</h1>
                            </Link>
                        </CardBody>
                    </Card>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages.messages
    };
};

const mapDispatchToProps = dispatch => {
    return {
        postMessage: message => dispatch(createMessage(message)),
        onFetchMessages: () => dispatch(fetchMessages())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);




