import {FETCH_MESSAGES_SUCCESS} from "../actions/actionTypes";

const initialState = {
    messages: []
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.messages};
        default:
            return state;
    }
};

export default messagesReducer;