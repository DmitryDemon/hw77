const fs = require('fs');

const filename = './db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContent = fs.readFileSync(filename);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    getItem(){
        return data;
    },
    addItem(item){
        data.push(item);
        this.save();
        return item;
    },
    save(){
        fs.writeFileSync(filename,JSON.stringify(data))
    }
};