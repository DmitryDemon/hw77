const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});



const router = express.Router();

router.get('/', (req, res) => {
    res.send(db.getItem())
});

router.post('/', upload.single('image'), (req, res) => {
    if  (!req.body.message){
        res.status(400).send({message: "Для отправки формы необходимо заполнить поле Message"})
    } else {
        const message = req.body;
        if (req.file) {
            console.log(req.file);
            message.image = req.file.filename;
        }
        db.addItem(message);
        res.send({message: 'OK'})
    }

});

module.exports = router;